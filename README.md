# cpp_mipt_fall_2021

Курс по С++ (и алгоритмам и структурам данных) на фопфе мфти, осень 2021, семинарист Нестюк Арсений

## Полезные ссылки

https://t.me/joinchat/nt2oaM8q9uEyMzZi -- чат курса

@deramerc -- личка семинариста в тг

https://google.com -- оракул, содержащий ответы на большую часть ваших вопросов


## Работа с гитлабом

Здесь и далее "репа" -- репозиторий, основной объект гитхаба/гитлаба, сборник кода конкретного проекта, его изменений, issues и всего прочего. Вы сейчас находитесь в моей репе, посвящённой курсу плюсов; чуть ниже по инструкции вы создадите свою

Что нужно сделать в первый раз:
* в этой репе нажать fork сверху справа
* заполнять необходимые поля и нажимать на кнопки "далее" и "создать" до тех пор, пока у вас не появится клон моей репы
* следовать инструкциям из раздела Solution статьи https://about.gitlab.com/blog/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/ -- благодаря этому изменения в моей репе будут автоматически подтягиваться в вашу

Что нужно сделать для сдачи домашнего задания:
* зайти в _свою_, форкнутую репу
* если сдаёте задание первый раз -- найти и тыкнуть плюсик слева от History, Find File, Web IDE, потом тыкнуть Create branch (создать ветку), осмысленно назвать ветку
* если не первый раз, то слева от плюса и названия репы есть выпадашка со списком веток, в ней нужно выбрать ветку текущего семинара
* тыкнуть на Web IDE
* зайти в папку problems/semX, где Х -- номер семинара
* создать в ней файл под названием, отражающим суть задачи, с расширением .cpp, и скопировать внутрь код вашего решения
* нажать Commit и следовать инструкциям
* (следующие пункты нужны только если вы сдаёте задачу первый раз; если не первый, тыкните меня в телегу)
* перейти на вкладку Merge requests из левого сайдбара и выбрать создание нового mr
* в качестве target branch выбрать мою репу, ветку main
* следовать инструкциям по созданию mr до конца. Выбрать название, отражающее, что именно вы пытаетесь сдать
