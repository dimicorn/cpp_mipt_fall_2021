#include <iostream>
#include <vector>

class Stack {
public:
    void Push(char c) {
        vector.push_back(c);
    };

    char Pop() {
        char c = vector.back();
        vector.pop_back();
        return c;
    };

    bool Empty() {
        return vector.empty();
    };

private:
    std::vector<char> vector;
};

char open_to_closed(char c) {
    if (c == '(') return ')';
    if (c == '[') return ']';
    if (c == '{') return '}';
    return 0;
}

int main() {
    int n;
    std::cin >> n;
    Stack stack;
    bool is_psp = true;
    for (int i = 0; (i < n) && is_psp; ++i) {
        char c;
        std::cin >> c;
        if (c == '(' || c == '[' || c == '{') {
            stack.Push(c);
            continue;
        }
        if ((c == ')' || c == ']' || c == '}') && !stack.Empty()) {
            if (c != open_to_closed(stack.Pop())) {
                is_psp = false;
            }
            continue;
        }
        is_psp = false;
    }
    if (is_psp && stack.Empty()) {
        std::cout << "YES" << std::endl;
    } else {
        std::cout << "NO" << std::endl;
    }
    return 0;
}

